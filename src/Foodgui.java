import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static javax.swing.JOptionPane.showMessageDialog;


public class Foodgui {
    private JPanel root;
    private JButton SioButton;
    private JButton SyoyuButton;
    private JButton MisoButton;
    private JButton sobaButton;
    private JButton gyouzaButton;
    private JButton gohanButton;
    private JButton checkOutButton;
    private JTextArea textbox1;
    private JTextPane textPane1;
    private JButton cancelButton;
    private JTextPane textPane2;

    int priceSum=0;
    int total=0;
    int todaytotal=0;

    void order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?  The price is " +price+" yen.",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation==0){
            textbox1.append(food+" "+price+"yen");
            textbox1.append(System.lineSeparator());
            JOptionPane.showMessageDialog(null,"Order for "+food+" recieved.");
            priceSum += price;
            textPane1.setText("Total  "+String.valueOf(priceSum)+"yen");
            total+=1;
        }
    }


    public Foodgui() {
        SioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 order("Sio",100);
            }
        });
        SyoyuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Syoyu",200);
            }
        });
        MisoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Miso",150);
            }
        });
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tonkotu",125);
            }
        });
        gyouzaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyouza",75);
            }
        });
        gohanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gohan",80);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation==0){
                    textbox1.setText(" ");
                    textPane1.setText(" ");
                    todaytotal+=total;
                    textPane2.setText("Total sales "+String.valueOf(todaytotal));
                    total=0;
                    JOptionPane.showMessageDialog(null,"Thank you.The total price is "+priceSum+" yen.");
                    priceSum=0;
                }
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Are you sure you want to cancel your order?",
                        "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation==0){
                textbox1.setText(" ");
                textPane1.setText(" ");
                priceSum=0;
                total=0;
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Foodgui");
        frame.setContentPane(new Foodgui().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
